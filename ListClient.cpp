#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2;
 
 L1.insert(78,1);
 L1.insert(45,2);
 L1.insert(33,3);
 L1.insert(22,1);
 L1.insert(90,4);
 L1.insert(111,1);
 cout<<L1.size()<<endl;
 cout<<L1.get(1)<<endl;
 cout<<L1.get(2)<<endl;
 cout<<L1.get(3)<<endl;
 cout<<L1.get(4)<<endl;
 cout<<L1.get(5)<<endl;
	L1.remove(3);
 cout<<L1.size()<<endl;
 cout<<L1.get(1)<<endl;
 cout<<L1.get(2)<<endl;
 cout<<L1.get(3)<<endl;
 cout<<L1.get(4)<<endl;
 cout<<endl<<endl;
 cout<<"L2"<<endl;
 L2.insert(5,1);
 L2.insert(10,2);
 L2.insert(58990,3);
 L2.insert(7777,1);
 L2.insert(60000,4);
 L2.insert(5000,1);
 cout<<L2.size()<<endl;
 cout<<L2.get(1)<<endl;
 cout<<L2.get(2)<<endl;
 cout<<L2.get(3)<<endl;
 cout<<L2.get(4)<<endl;
 cout<<L2.get(5)<<endl;
	L2.remove(3);
 cout<<L2.size()<<endl;
 cout<<L2.get(1)<<endl;
 cout<<L2.get(2)<<endl;
 cout<<L2.get(3)<<endl;
 cout<<L2.get(4)<<endl;
 
 

 //Do some stuff with L1, L2, ...
 // ...

}
